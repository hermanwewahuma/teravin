import React, {Component} from 'react';
import {
    View,
    Animated,
    Image,
    Easing,
    StatusBar,
    AsyncStorage,
    Text,
    Alert,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import NetInfo from "@react-native-community/netinfo";  

import Modal from 'react-native-modal';

const version = require('../../../package.json').version;

let animationTimeout;
let timer = 3000;

const deviceWidth = Dimensions.get("window").width;

export default class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
        this.springValue = new Animated.Value(0.3);
        this.state = {
            login: false,
            doneOnboarding:0,
            visibleModal : false
        }
    }

    componentDidMount() {   
        this.animate();

        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
            if(state.isConnected == false){
                this.setState({
                    visibleModal : true
                })

                setTimeout(()=>{
                    this.setState({
                        visibleModal : false
                    });     
                }, 2000);
            }
            
        });

       setTimeout(()=>{
        Actions.tab_home();
       },3000)
    }

    animate () {
        this.animatedValue.setValue(0)
        Animated.timing(
          this.animatedValue,
          {
            useNativeDriver: true,
            toValue: 1,
            duration: 2000,
            easing: Easing.linear
          }
        ).start(() => this.animate())
      }


    render() {

        const opacity = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 1, 0]
        })
        
        return (
            <View style={{ flex: 1, backgroundColor: '#ffff', justifyContent: 'center', alignItems: 'center' }}>
                <StatusBar
                    hidden = {false}
                    backgroundColor = {"#ffff"}
                    translucent = {false}
                    networkActivityIndicatorVisible = {true}
                />

                <Animated.View style={{opacity, height: 400, width: '80%',justifyContent: 'center', alignItems: 'center'}} >
                    <Text style={{ fontSize:55, fontWeight:"bold", color:"#c48d0c" }}>HWH</Text>
                </Animated.View>
                <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0 }}>
                    <Text small style={{ color: "#c48d0c"}}>Version {version} </Text>
                </View>

                <Modal 
                        isVisible={this.state.visibleModal}
                        animationOut={"bounceOutDown"}
                        animationOutTiming={1000}
                        backdropColor={"transparent"}
                        style={styles.bottomModal}
                        deviceWidth={deviceWidth}
                    >
                        <View style={styles.modal}>
                            <View>
                                <Text style={styles.txt}>Check Your</Text>
                                <Text style={styles.txt}>Connection Internet</Text>
                            </View>
                            <View>
                                <TouchableOpacity
                                    style={styles.btnShow}
                                    onPress={()=>{
                                        this.setState({
                                            visibleModal : false
                                        })
                                    }}
                                >
                                    <Text>OK</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
     bottomModal: {
         justifyContent: 'flex-end',
         margin: 0,
         left:5,
         marginRight:10
     },
     modal : {
         backgroundColor:"#ffff",
         height:100,
         flexDirection:"row",
         alignItems:"center",
         justifyContent:"space-around",
         bottom:10,
         borderRadius:5,
     },
     btnShow : {
         padding:5,
         paddingLeft:10,
         paddingRight:10,
         borderWidth:1,
         borderColor:"#c48d0c"
     },
     txt : {
         fontWeight:"bold",
         fontSize:20
     }
 });