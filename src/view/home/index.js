import React, {Component} from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ActivityIndicator,
    RefreshControl,
    SafeAreaView,
    Alert,
    ToastAndroid
} from 'react-native';

import Header from './../../components/header'

import IconFeather from 'react-native-vector-icons/Feather'; 
import IconEvilIcons from 'react-native-vector-icons/EvilIcons'; 

import NetInfo from "@react-native-community/netinfo";  

import Modal from 'react-native-modal';

const deviceWidth = Dimensions.get("window").width;

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading         : false,
            data            : [],
            titleCart       : [],
            valTitleCart    : "",
            show            : false,
            visibleModal    : false
        }
    }

    componentDidMount() {     
        this._getMovie();

        NetInfo.fetch().then(state => {
            if(state.isConnected == true){
                this.timer = setInterval(()=>{
                    this.setState({
                        visibleModal: true
                    });

                    this._getMovie();  

                    setTimeout(()=>{
                        this.setState({
                            visibleModal: false
                        })
                    },10000)
                }, 60000)
            }else{
                this.setState({
                    loading: false
                })
            }
            
        });

        
    }
    
    componentWillUnmount(){
        clearInterval(this.timer);
    }

    _renderItem = ({item, index}) => {
        return (
            <View style={{ width:"100%" }}>
                <Image 
                style={{ }}
                    source={item.image}
                />
            </View>
        );
    }

    _getMovie=()=>{
        this.setState({
            loading:true
        })

        try {           
            fetch(`https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf`,{
                    method :"GET"
                })
                .then(response => response.json())
                .then(res => {
                    this.setState({
                        loading:false
                    })
                    if(res.results.length > 0) {
                        this.setState({
                            data : res.results
                        })
                    }
                })
        } catch (error) {
            this.setState({
                loading:false
            })
        }
    }

    _play=(v)=>{
        ToastAndroid.showWithGravityAndOffset(
            `COMING SOON`,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            0,
            5
          );
    }

    _schedule=()=>{
        
    }

    _renderItem=(item)=>{
        return(
            <View style={styles.item}>
                <Image
                    style={{width:"100%"}}
                    // source={ item.image }
                    resizeMode={"contain"}
                />                
                <View style={styles.itemList}>
                    <TouchableOpacity
                        onPress={()=> {this._play()}}
                    >
                        <IconEvilIcons
                            name={"play"}
                            size={30}
                            color={"#ffff"}
                            style={{  }}
                        />
                    </TouchableOpacity>
                    <View style={styles.movie}>
                        <Text style={{ color:"#ffff", marginLeft:5 }}>{item.original_title}</Text>
                        <Text style={{ color:"#ffff", marginLeft:5 }}>{item.release_date}</Text>
                    </View>
                </View>
            </View>
        )
    }

    _rendernoData=()=>{
        return(
            <View style={{padding :10, alignItems:"center"}}>
                <Text style={{color:"grey"}}>There is no data!</Text>
            </View>
        )
    }

    render() {

        const{valTitleCart}= this.state;
        const screenWidth = Math.round(Dimensions.get('window').width);

        return (
            <View style={{ flex: 1, backgroundColor: '#e6e5e3', borderTopRightRadius:190 }}>
                <StatusBar
                    hidden = {false}
                    backgroundColor = {"#c48d0c"}
                    translucent = {false}
                    networkActivityIndicatorVisible = {true}
                    inactiveSlideScale={0.99}
                    inactiveSlideOpacity={0.8}
                    enableMomentum={false}
                />
                <Header
                    menu={true}
                    cart={true}
                    titleCart={valTitleCart}
                />
                    <View style={styles.body}>
                        <View style={{ marginBottom:20 }}>
                            <View style={{ marginLeft:10, marginTop:10 }}>
                                <Text style={{ fontWeight:"bold", fontSize:18 }}>Top Movies</Text>
                            </View>
                        </View>
                            <View style={{ flexDirection:"row" }}>
                                <FlatList
                                    style={{ flex : 1, marginBottom:165}}
                                    data={this.state.data}
                                    renderItem={({ item }) => (this._renderItem(item))}
                                    enableEmptySections={true}
                                    ListEmptyComponent={()=>(this._rendernoData())}
                                    keyExtractor={(item, index) => index.toString()}
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.loading}
                                            onRefresh={() => this._getMovie()}
                                        />
                                    }
                                /> 
                            </View>
                    </View>

                    <Modal 
                        isVisible={this.state.visibleModal}
                        animationOut={"bounceOutDown"}
                        animationOutTiming={1000}
                        backdropColor={"transparent"}
                        style={styles.bottomModal}
                        deviceWidth={deviceWidth}
                    >
                        <View style={styles.modal}>
                            <View>
                                <Text style={styles.txt}>Local storage</Text>
                                <Text style={styles.txt}>has been updated</Text>
                            </View>
                            <View>
                                <TouchableOpacity
                                    style={styles.btnShow}
                                >
                                    <Text>SHOW</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
            </View>
        );
    }
}


const styles = StyleSheet.create({
   item :{
       backgroundColor:"#ffff",
       borderRadius:10,
       elevation:1,
       marginLeft:5,
       marginRight:5,
       marginBottom:5
   },
   itemList :{
       flexDirection:'row', 
       alignItems:'center', 
       padding:7, 
       backgroundColor:"grey",
       borderBottomLeftRadius:10,
       borderTopRightRadius:10
    },
    movie : {
        flexDirection:"column"
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
        left:5,
        marginRight:10
    },
    modal : {
        backgroundColor:"#ffff",
        height:100,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-around",
        bottom:50,
        borderRadius:5,
    },
    btnShow : {
        padding:5,
        paddingLeft:10,
        paddingRight:10,
        borderWidth:1,
        borderColor:"#c48d0c"
    },
    txt : {
        fontWeight:"bold",
        fontSize:20
    }
});

// https://gitlab.com/hermanwewahuma/teravin.git