import React, {Component} from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
} from 'react-native';

import Header from'./../../components/header'

export default class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {     
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    hidden = {false}
                    backgroundColor = {"#c48d0c"}
                    translucent = {false}
                    networkActivityIndicatorVisible = {true}
                />
                
                <Header
                    menu={true}
                    Title={"MY ACCOUNT"}
                />

            </View>
        );
    }
}