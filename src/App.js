

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    BackHandler,
    Platform,
    NetInfo,
    ToastAndroid
} from 'react-native'

import {
    Scene,
    Router,
    Actions,
    Reducer,
    ActionConst,
    Overlay,
    Tabs,
    Modal,
    Drawer,
    Stack,
    Lightbox,
} from "react-native-router-flux"

import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Favorite from './view/favorite'
import Home from './view/home'
import Account from './view/account'
import SplashScreen from './view/splash'

const getSceneStyle = () => ({
  backgroundColor: "#EEEEEE",
  shadowOpacity: 100,
  shadowRadius: 3
});

let exitCount = 0;

const onBackPress = () => {
  let route = Actions.state.routes;
  let topSection = route[route.length - 1]
  let section = topSection.routes;
  switch (route[route.length - 1].routeName) {
      case "_main":

          let tab = route[route.length - 1].routes[0].index;
          if(tab == 0) {
              exitCount++;
              ToastAndroid.showWithGravityAndOffset(
                  'Tekan lagi untuk keluar',
                  ToastAndroid.SHORT,
                  ToastAndroid.BOTTOM,
                  25,
                  100
              );
                  if(exitCount == 2) {
                      exitCount = 0
                      BackHandler.exitApp()
                  }
          }
          else {
              exitCount = 0;
              Actions.pop();     
          }

          return true;
          break;
  }
  
}

export default class App extends Component {

  /**
   * @function constructor
   * @param {object} props 
   */
  constructor(props) {
      super(props);
      this.state = {

      }
  }

  render() {
      return (
          <Router
              getSceneStyle={getSceneStyle}
              backAndroidHandler={onBackPress}
              >
              <Stack
                  hideNavBar
                  key="root"
                  titleStyle={{ alignSelf: 'center' }}                  
                >
                  <Scene key="_splash" component={SplashScreen} hideNavBar hideTabBar initial />
                  <Scene key="_main" hideNavBar type={ActionConst.REPLACE} panHandlers={null} >

                  <Tabs  
                      activeTintColor={"#c48d0c"}   
                      key="tabbar" 
                      tabBarPosition={'bottom'}
                      >

                          <Stack                                
                              key="tab_home"
                              title="Home"
                              tabBarLabel="Home"
                              icon={_iconHome}
                              >
                              <Scene key="Home" component={Home} title="Home" initial hideNavBar />
                          </Stack>
                          
                          <Stack                                
                              key="tab_favorite"
                              title="Favorite"
                              tabBarLabel="Favorite"
                              icon={_iconFavorite}
                              >
                              <Scene key="Favorite"  component={Favorite} title="Favorite" initial hideNavBar />
                          </Stack>

                          <Stack                                
                              key="tab_account"
                              title="Account"
                              tabBarLabel="Account"
                              icon={_iconAccount}
                              >
                              <Scene key="account" component={Account} title="Account" initial hideNavBar hideTabBar={false} />

                          </Stack>
                      </Tabs>
                  </Scene>

              </Stack>
          </Router>
      );
  }
}

let size = 20;
let top = 7 ;

function _iconHome(props) {
  return(
      <IconSimpleLine size={size}
          name={ Platform.OS == "ios" ? "home" : "home"}
          color={props.focused ? "#c48d0c" : '#9aa69c' }
          style={{paddingTop:top}}
      />
  )
}

function _iconFavorite(props) {
return(
    <IconMaterialIcons size={size}
         name={ Platform.OS == "ios" ? "favorite-outline" : "favorite-outline"}
         color={props.focused ? "#c48d0c" : '#9aa69c' }
         style={{paddingTop:top}}
         size={25}
    />
  )
}

function _iconAccount(props) {
  return(
      <IconSimpleLine size={size}
          name={ Platform.OS == "ios" ? "user" : "user"}
          color={props.focused ? "#c48d0c" : '#9aa69c' }
          style={{paddingTop:top}}
      />
  )
}

